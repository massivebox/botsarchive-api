module codeberg.org/massivebox/botsarchive-api

go 1.17

require (
	github.com/json-iterator/go v1.1.11
	github.com/valyala/fasthttp v1.29.0
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
